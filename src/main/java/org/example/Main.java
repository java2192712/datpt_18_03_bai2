package org.example;

public class Main {
    public static void main(String[] args) {
        Animals animals = new Animals("Nigger", 41, "Vang");
        animals.sounding(); // in ra Gâu Gâu
        Dogs dogs = new Dogs();
        Animals dogsAnimals = new Dogs();
        dogsAnimals.sounding(); // in ra Gâu Gâu Gâu
        dogs.sounding("Meo Meo"); // in ra Meo Meo
    }

    //Do class con sử dụng private để định nghĩa các biến và phương thức nên không thể gọi đến các biến và phương thức được
    // -> muốn gọi được các biến thì cần phải sử dụng public
    // và khi tạo biến ở class khác và muốn bảo mật nhưng vẫn muốn sử dụng ở class khác thì cẩn phải sử getter, setter cho các biến
    // và để public (getter dùng để lấy ra xem, setter sẽ cập nhật giá trị mới cho biến

    // Class Animals có khởi tạo constructor có tham số nên cần phải khởi tạo constructor không tham số cho class animals
    // thì class Dogs kế thừa mới có thể tạo ra các biến hay phương thức mà chạy không lỗi
}